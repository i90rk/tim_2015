from flask import Flask, redirect, url_for, render_template, jsonify

import pandas as pd
import numpy as np
import json
import math

app = Flask(__name__)

def calc_centroid(geometries):
    x = 0
    y = 0
    for i in range(0, len(geometries) - 1):
        x += geometries[i][0]
        y += geometries[i][1]

    x /= len(geometries) - 1
    y /= len(geometries) - 1

    return [x, y]

def calc_distance(point1, point2):
    distance = math.sqrt(math.pow((point1[0] - point2[0]), 2) + math.pow((point1[0] - point2[0]), 2))
    return distance

def calc_area(geometries):
	# http://www.movable-type.co.uk/scripts/latlong.html
	if len(geometries) > 0:
		# length of a
		lon1 = geometries[0][0]
		lat1 = geometries[0][1]
		lon2 = geometries[1][0]
		lat2 = geometries[1][1]

		lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])
		# haversine formula 
		dlon = lon2 - lon1 
		dlat = lat2 - lat1 
		a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
		c = 2 * math.asin(math.sqrt(a)) 
		a_km = 6367 * c

		# length of b
		lon1 = geometries[1][0]
		lat1 = geometries[1][1]
		lon2 = geometries[2][0]
		lat2 = geometries[2][1]

		lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])
		# haversine formula 
		dlon = lon2 - lon1 
		dlat = lat2 - lat1 
		a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
		c = 2 * math.asin(math.sqrt(a)) 
		b_km = 6367 * c


		area = a_km * b_km
	else:
		area = 1

	return area

def divide_with_area(x, value_column, square_area_column):
	return x[value_column] / x[square_area_column]

def process_revenues(area, grid_path, center_square_id):
	layer_radius = {
	    'first': 0.04,
	    'second': 0.08,
	    'third': 0.14,
	    'fourth': 0.22,
	    'fifth': 0.32,
	    'sixth': 0.44,
	}

	data = pd.read_csv('tim_data_processed/revenues_per_square_id.csv')
	data = data[(data.area == area)]
	data = data.values.tolist()

	with open(grid_path) as json_data:
	    grid = json.load(json_data)

	features = grid['features']
	center_polygon_geometries = list()
	center_polygon_centroid = list()
	for i in range(0, len(features)):
	    if features[i]['properties']['id'] == center_square_id:
	        center_polygon_geometries = features[i]['geometry']['coordinates'][0]
	        break

	center_polygon_centroid = calc_centroid(center_polygon_geometries)

	for i in range(0, len(data)):
	    polygon_geometries = list()
	    polygon_centroid = list()
	    distance = float(0)
	    polygon_area = float(0)

	    for j in range(0, len(features)):
	        if features[j]['properties']['id'] == data[i][1]:
	            polygon_geometries = features[j]['geometry']['coordinates'][0]
	            break

	    polygon_centroid = calc_centroid(polygon_geometries)
	    polygon_area = calc_area(polygon_geometries)
	    distance = calc_distance(center_polygon_centroid, polygon_centroid)

	    data[i].append(polygon_area) # append square area

	    if distance < layer_radius['first']:
	        data[i].append(1)
	    elif distance >= layer_radius['first'] and distance < layer_radius['second']:
	        data[i].append(2)
	    elif distance >= layer_radius['second'] and distance < layer_radius['third']:
	        data[i].append(3)
	    elif distance >= layer_radius['third'] and distance < layer_radius['fourth']:
	        data[i].append(4)
	    elif distance >= layer_radius['fourth'] and distance < layer_radius['fifth']:
	        data[i].append(5)
	    elif distance >= layer_radius['fifth'] and distance < layer_radius['sixth']:
	        data[i].append(6)
	    else:
	        data[i].append(7)

		data[i][2] = long(data[i][2])

	df = pd.DataFrame(data, columns=['area', 'square_id', 'revenue', 'square_area', 'layer'])
	df = df.groupby(['layer'], as_index=False).agg({'revenue': np.sum, 'square_area': np.sum})

	division = df[['layer', 'square_area', 'revenue']].apply(divide_with_area, axis=1, args=('revenue', 'square_area'))
	df['division'] = division
	return df

@app.route('/')
def hello_world():
    return redirect(url_for('companies_layers'))

@app.route('/companies_layers/')
@app.route('/companies_layers/<city>')
def companies_layers(city=None):
	area = ''
	geo_path = ''
	center_polygon = ''

	if city == None:
		city = 'bari'

	if city == 'bari':
		area = 'Bari'
		geo_path = 'app/static/geojson/grid_bari.geojson'
		center_polygon = '2321_3_2_0_1'
	elif city == 'milano':
		area = 'Milano'
		geo_path = 'app/static/geojson/grid_milano.geojson'
		center_polygon = '3950_3_1_2_1_2_1'
	elif city == 'napoli':
		area = 'Napoli'
		geo_path = 'app/static/geojson/grid_napoli.geojson'
		center_polygon = '2026_3_2_0_2_0'
	elif city == 'palermo':
		area = 'Palermo'
		geo_path = 'app/static/geojson/grid_palermo.geojson'
		center_polygon = '1275_3_2_3_1'
	elif city == 'roma':
		area = 'Roma'
		geo_path = 'app/static/geojson/grid_roma.geojson'
		center_polygon = '3420_0_2_3_0_2'
	elif city == 'torino':
		area = 'Torino'
		geo_path = 'app/static/geojson/grid_torino.geojson'
		center_polygon = '3877_1_3_3_1_0_0'
	elif city == 'venezia':
		area = 'Venezia'
		geo_path = 'app/static/geojson/grid_venezia.geojson'
		center_polygon = '3693_0_0_0_2'
	else:
		area = 'Bari'
		geo_path = 'app/static/geojson/grid_bari.geojson'
		center_polygon = '2321_3_2_0_1'

	process_data = process_revenues(area, geo_path, center_polygon)
	
	data = {'city': city, 'process_data': process_data.values.tolist(), 'area': area}
	return render_template('companies_layers.html', data=data)

@app.route('/presence/')
@app.route('/presence/<city>/<type>')
def presence(city=None, type=None):
	if city == None:
		city = 'bari'
		type = 'residents'

	file_path = 'tim_data_processed/presence-' + city + '.csv'
	geo_path = 'app/static/geojson/grid_' + city + '.geojson'

	with open(geo_path) as json_data:
	    grid = json.load(json_data)

	pdata = pd.read_csv(file_path)
	pdata = pdata[['square_id', type]].sort_values(by=type, ascending=1)
	pdata = pdata.values.tolist()

	features = grid['features']

	for i in range(0, len(pdata)):
	    polygon_geometries = list()
	    polygon_area = float(0)

	    for j in range(0, len(features)):
	        if features[j]['properties']['id'] == pdata[i][0]:
	            polygon_geometries = features[j]['geometry']['coordinates'][0]
	            break

	    polygon_area = calc_area(polygon_geometries)
	    pdata[i].append(polygon_area) # append square area

	df = pd.DataFrame(pdata, columns=['square_id', type, 'square_area'])
	division = df.apply(divide_with_area, axis=1, args=(type, 'square_area'))
	df['division'] = division
	df = df[['square_id', type, 'division']].sort_values(by='division', ascending=1)

	process_data = df.values.tolist()

	data = {'city': city, 'type': type, 'process_data': process_data}
	return render_template('presence.html', data=data)

@app.route('/companies/')
@app.route('/companies/<city>')
def companies(city=None, type=None):
	if city == None:
		city = 'bari'

	area = city.title()
	file_path = 'tim_data_processed/revenues_per_square_id.csv'
	geo_path = 'app/static/geojson/grid_' + city + '.geojson'
	
	with open(geo_path) as json_data:
	    grid = json.load(json_data)

	pdata = pd.read_csv(file_path)
	pdata = pdata[(pdata.area == area)]
	pdata = pdata.values.tolist()

	features = grid['features']

	for i in range(0, len(pdata)):
	    polygon_geometries = list()
	    polygon_area = float(0)

	    for j in range(0, len(features)):
	        if features[j]['properties']['id'] == pdata[i][1]:
	            polygon_geometries = features[j]['geometry']['coordinates'][0]
	            break

	    polygon_area = calc_area(polygon_geometries)
	    pdata[i].append(polygon_area) # append square area
	    pdata[i][2] = long(pdata[i][2])

	df = pd.DataFrame(pdata, columns=['area', 'square_id', 'revenue', 'square_area'])
	division = df.apply(divide_with_area, axis=1, args=('revenue', 'square_area'))
	df['division'] = division
	df = df.sort_values(by='division', ascending=1)

	process_data = df.values.tolist()

	data = {'city': city, 'area': area, 'process_data': process_data}
	return render_template('companies.html', data=data)

if __name__ == '__main__':
    app.run()