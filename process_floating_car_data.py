import pandas as pd
import numpy as np
import time
import datetime

import requests
import random
import xml.etree.ElementTree as ET
from geolocation import GeoLocation

''' 
	Execute this script with nohup as background process because it can last
	up to 3 hours when processing only 4000 records

	nohup python -u process_floating_car_data.py &
'''

'''
    Only 5 files are procesed for Torino and they are chosen according to moon visibility:
    |---------------|-----------------|
    | Date          | Moon visibility |
    |---------------|-----------------|
    | 20.03.2015    | 0%              |
    |---------------|-----------------|
    | 27.03.2015    | 50%             |
    |---------------|-----------------|
    | 04.04.2015    | 100%            |
    |---------------|-----------------|
    | 12.04.2015    | 50%             |
    |---------------|-----------------|
    | 18.04.2015    | 0%              |
    |---------------|-----------------|
'''



# start properties
# change this properties to process different cities
city = 'torino'
date = '20150418' # year, month, day
input_path = 'tim_data/output_' + date + '_' + city.upper() + '.csv'
output_path = 'tim_data_processed/floating_car_data_' + date + '_' + city.upper() + '.csv'
max_count = 4000 # max number of rows to be processed
# end properties

float_car_columns = ['travell_id', 'timestamp', 'latitude', 'longitude', 'vehicle_category', 'speed']
float_car_data = pd.read_csv(input_path,
    names=float_car_columns,
    header=None,
    sep=';')

float_car_data = float_car_data[(float_car_data.speed > 30)]
float_car_data_list = float_car_data.values.tolist()

count = 0 # number of processed rows
data = []

random.seed()
while True:
    random_item = random.choice(float_car_data_list)
    # this row is already processed
    if len(random_item) > 6:
        continue

    latitude = random_item[2]
    longitude = random_item[3]
    acquired_speed = random_item[5]

    # http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
    loc = GeoLocation.from_degrees(latitude, longitude)
    distance = 0.5  # 500 meters

    SW_loc, NE_loc = loc.bounding_locations(distance)

    minlon = str(SW_loc.deg_lon)
    minlat = str(SW_loc.deg_lat)
    maxlon = str(NE_loc.deg_lon)
    maxlat = str(NE_loc.deg_lat)

    # http://wiki.openstreetmap.org/wiki/Xapi#Query_Map
    url = 'http://www.overpass-api.de/api/xapi?*[bbox=' + minlon + ',' + minlat + ',' + maxlon + ',' + maxlat + '][maxspeed=*]'
    r = requests.get(url)

    root = ET.fromstring(r.content)

    maxspeed_allowed = 0
    minspeed_allowed = 0

    if len(root.findall(".//tag[@k='maxspeed']")) > 0:
        for i in root.findall(".//tag[@k='maxspeed']"):
            if int(i.get('v')) > maxspeed_allowed:
                maxspeed_allowed = int(i.get('v'))

        minspeed_allowed = maxspeed_allowed
        for i in root.findall(".//tag[@k='maxspeed']"):
            if int(i.get('v')) < minspeed_allowed:
                minspeed_allowed = int(i.get('v'))

        offense = 3
        if maxspeed_allowed == minspeed_allowed:
            if acquired_speed > maxspeed_allowed:
                offense = 1 # definite offense
            else:
                offense = 2 # no offense
        else:
            if acquired_speed > maxspeed_allowed:
                offense = 1 # definite offense
            elif acquired_speed <= minspeed_allowed:
                offense = 2 # no offense
            else:
                offense = 3 # maybe

        random_item.append(offense)
        data.append(random_item)

        count += 1
        # print time in nohup output file when a row is processed
        print str(count) + ": " + time.strftime("%d/%m/%Y %H:%M:%S")

    if count == max_count:
        break

df = pd.DataFrame(data, columns=['travell_id', 'timestamp', 'latitude', 'longitude', 'vehicle_category', 'speed', 'offense'])
df.to_csv(
    output_path,
    index=False,
    mode='w+')