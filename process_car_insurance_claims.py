import pandas as pd
import numpy as np
import json

def point_in_polygon(point, polygon):
    # https://github.com/substack/point-in-polygon

    # coordinates are in reverse order
    x = point[0]
    y = point[1]

    inside = False
    j = len(polygon) - 2
    for i in range(0, (len(polygon)-1)):
        # coordinates are in reverse order
        xi = polygon[i][1]
        yi = polygon[i][0]
        
        xj = polygon[j][1]
        yj = polygon[j][0]

        intersect = ((yi > y) != (yj > y)) & (x < (xj - xi) * (y - yi) / (yj - yi) + xi)

        if intersect:
            inside = not inside

        j = i

    return inside

def normalize_data(x, population, per):
    # calculates car accidents rate --> (number of accidents / population) x 100000 = Accidents Rate per 100000 inhabitants
    return (float(x) / float(population)) * float(per)

# start properties
# change this properties to process different cities
city = 'venezia'
cities_population = {
    'bari': 1000000,
    'milano': 3196825,
    'napoli': 3128700,
    'palermo': 1300000,
    'roma': 4321244,
    'torino': 2200000,
    'venezia': 272000
}
cities_codes = {
    'bari': 'BA',
    'milano': 'MI',
    'napoli': 'NA',
    'palermo': 'PA',
    'roma': 'RM',
    'torino': 'TO',
    'venezia': 'VE'
}
norm_rate = 100000
grid_geojson_path = 'cities_grids_geojson/grid_' + city + '.geojson'
input_path = 'tim_data/BDC2015_UnipolsaiClaims2014_' + cities_codes[city] + '.csv'
output_path_day_hour = 'tim_data_processed/car_insurance_claims_' + city + '_day_hour.csv'
output_path_day = 'tim_data_processed/car_insurance_claims_' + city + '_day.csv'
output_path_square_id = 'tim_data_processed/car_insurance_claims_' + city + '_square.csv'
# end properties

process_data = pd.read_csv(input_path)

# number of crashes per day and hour, normalized
agg_day_hour = pd.DataFrame({'num_crashes' : process_data.groupby(['day_type', 'time_range'], as_index=False)\
        .size()})\
    .reset_index()

num_crashes_norm = agg_day_hour['num_crashes']\
    .apply(normalize_data, args=(cities_population[city], norm_rate))

agg_day_hour['num_crashes_norm'] = num_crashes_norm
agg_day_hour.to_csv(
    output_path_day_hour,
    index=False,
    mode='w+')

# number of crashes per day, normalized
agg_day = pd.DataFrame({'num_crashes' : process_data.groupby(['day_type'], as_index=False)\
        .size()})\
    .reset_index()

num_crashes_norm = agg_day['num_crashes']\
    .apply(normalize_data, args=(cities_population[city], norm_rate))

agg_day['num_crashes_norm'] = num_crashes_norm
agg_day.to_csv(
    output_path_day,
    index=False,
    mode='w+')

# number of crashes per square id
with open(grid_geojson_path) as json_data:
    gg_data = json.load(json_data)

features = gg_data['features']
process_data_list = process_data.values.tolist()
square_ids = list()

for i in range(0, len(process_data_list)):
    record = process_data_list[i]
    longitude = record[0]
    latitude = record[1]

    for j in range(0, len(features)):
        square_id = features[j]['properties']['id']
        polygon = features[j]['geometry']['coordinates'][0]

        inside = point_in_polygon([latitude, longitude], polygon)

        if inside:
            square_ids.append(square_id)
            break

    # square_id not found
    if not inside:
        square_ids.append(0)

process_data['square_id'] = square_ids
agg_square_id = pd.DataFrame({'num_crashes' : process_data.groupby(['square_id'], as_index=False)\
        .size()})\
    .reset_index()

agg_square_id.to_csv(
    output_path_square_id,
    index=False,
    mode='w+')