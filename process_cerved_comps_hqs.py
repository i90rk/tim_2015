import pandas as pd
import numpy as np
import time
import datetime
import json
import math

''' 
    Execute this script with nohup as background process because depending on the hardware
    it can last up to 80 hours

    nohup python -u process_cerved_comps_hqs.py &
'''

def point_in_polygon(point, polygon):
    # https://github.com/substack/point-in-polygon

    # coordinates are in reverse order
    x = point[0]
    y = point[1]

    inside = False
    j = len(polygon) - 2
    for i in range(0, (len(polygon)-1)):
        # coordinates are in reverse order
        xi = polygon[i][1]
        yi = polygon[i][0]
        
        xj = polygon[j][1]
        yj = polygon[j][0]

        intersect = ((yi > y) != (yj > y)) & (x < (xj - xi) * (y - yi) / (yj - yi) + xi)

        if intersect:
            inside = not inside

        j = i

    return inside

def find_grid_square_id(data):
    features = grids_data[data[5]]['features']

    result = None
    longitude = data[2]
    latitude = data[3]

    for i in range(0, len(features)):
        square_id = features[i]['properties']['id']
        polygon = features[i]['geometry']['coordinates'][0]

        inside = point_in_polygon([latitude, longitude], polygon)

        if inside:
            result = square_id
            break

    # square_id not found
    if not inside:
        result = '0'

    return result


companies_file_path = 'tim_data/cerved-companies.csv'
headquarters_file_path = 'tim_data/headquarters-full.csv'

output_path = 'tim_data_processed/revenues_per_square_id_test.csv'

bari_grid_geo__path = 'cities_grids_geojson/grid_bari.geojson'
milano_grid_geo__path = 'cities_grids_geojson/grid_milano.geojson'
napoli_grid_geo__path = 'cities_grids_geojson/grid_napoli.geojson'
palermo_grid_geo__path = 'cities_grids_geojson/grid_palermo.geojson'
roma_grid_geo__path = 'cities_grids_geojson/grid_roma.geojson'
torino_grid_geo__path = 'cities_grids_geojson/grid_torino.geojson'
venezia_grid_geo__path = 'cities_grids_geojson/grid_venezia.geojson'

# cerved-companies.csv columns -> (subject_id,company_name,ateco_code,dimension,company_age)
companies = pd.read_csv(companies_file_path)
companies_list = companies.values.tolist()

# headquarters-full.csv columns -> (subject_id;sign;long;lat;kind;area)
headquarters = pd.read_csv(headquarters_file_path, sep=';')
headquarters['revenue'] = float(0)
headquarters['square_id'] = None

# bari grid
with open(bari_grid_geo__path) as json_data:
    bari_grid = json.load(json_data)

# milano grid
with open(milano_grid_geo__path) as json_data:
    milano_grid = json.load(json_data)

# napoli grid
with open(napoli_grid_geo__path) as json_data:
    napoli_grid = json.load(json_data)

# palermo grid
with open(palermo_grid_geo__path) as json_data:
    palermo_grid = json.load(json_data)

# roma grid
with open(roma_grid_geo__path) as json_data:
    roma_grid = json.load(json_data)

# torino grid
with open(torino_grid_geo__path) as json_data:
    torino_grid = json.load(json_data)

# venezia grid
with open(venezia_grid_geo__path) as json_data:
    venezia_grid = json.load(json_data)

revenue_codes = {
    'micro': 2000000,
    'piccola': 10000000,
    'media': 50000000,
    'grande': 250000000,
    'n.d.': 0
}
grids_data = {
    'Bari': bari_grid,
    'Milano': milano_grid,
    'Napoli': napoli_grid,
    'Palermo': palermo_grid,
    'Roma': roma_grid,
    'Torino': torino_grid,
    'Venezia': venezia_grid,
}

for i in range(0, len(companies_list)):
    heads_branches = headquarters[(headquarters.subject_id == companies.values[i][0])]
    heads_branches_list = heads_branches.values.tolist()

    if len(heads_branches_list) > 1:
        heads_count = 0
        branches_count = 0
        for j in range(0, len(heads_branches_list)):
            # S - headquarters, U - local unit
            if heads_branches_list[j][4] == 'S':
                heads_count += 1

            if heads_branches_list[j][4] == 'U':
                branches_count += 1

        funds = revenue_codes[companies_list[i][3]] / (branches_count + heads_count * math.sqrt(branches_count))
        for row in heads_branches.iterrows():
            index, data = row
            data_list = data.values.tolist()
            square_id = find_grid_square_id(data_list)
            headquarters.set_value(index, 'square_id', square_id)
            if data[4] == 'S':
                head_funds = math.sqrt(branches_count) * funds
                headquarters.set_value(index, 'revenue', head_funds)
            else:
                headquarters.set_value(index, 'revenue', funds)
    else:
        for row in heads_branches.iterrows():
            index, data = row
            data_list = data.values.tolist()
            square_id = find_grid_square_id(data_list)
            headquarters.set_value(index, 'revenue', float(revenue_codes[companies_list[i][3]]))
            headquarters.set_value(index, 'square_id', square_id)

    print str(i) + ": " + time.strftime("%d/%m/%Y %H:%M:%S")

print 'finish'

group_by_square_id = headquarters.groupby(['area', 'square_id'], as_index=False)\
    .agg({'revenue': np.sum})\
    .sort_values(by='area')

group_by_square_id.to_csv(
    output_path,
    index=False,
    mode='w+')