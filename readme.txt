--- UPATSTVO ZA INSTALACIJA NA APLIKACIJATA (Linux i python 2.7) ---

1. Prevzemanje na proektot od git
2. Vnatre vo proektot se kreira virtuelna okolina
	- vo slucaj da ne e instalirana python okolina
		- sudo apt-get install build-essential python-dev
		- sudo apt-get install python-pip
		- sudo pip install virtualenv

	* cd patekata_do_proektot/
	* virtualenv venv (se kreira virtuelna okolina)
3. Aktiviranje na virtuelnata okolina
	* source venv/bin/activate
4. Instalacija na bibliotekite
	* pip install -r requirements.txt (ova mora da se izvrsi vo aktivirana virtuelna okolina)
5. Start na aplikacijata
	* python run.py
6. Linkot se otvora vo browser (linkot od serverot daden vo terminalot)



--- ZA OBRABOTKA NA PODATOCITE ---

1. Potrebno e da se kreira folder tim_data na root vo proektot
2. Vo ovoj folder treba da gi ima csv datotekite dadeni na slikata listaNaCsvDatotekiVoFolderot_tim_data.png
3. Sekoja od skriptite process_car_insurance_claims.py, process_cerved_comps_hqs.py, process_floating_car_data.py, process_presence.py
	se izvrsuva za obrabotka na podatocnite mnozestva Car Insurance Claims, Cerved Companies and Headquarters,
	Floating Car Data i Presence soodvetno
