import pandas as pd
import numpy as np
import time
import datetime

# start properties
# change this properties to process different cities
city = 'venezia'
input_path = 'tim_data/presence-' + city + '.csv'
output_path = 'tim_data_processed/presence-' + city + '.csv'
# end properties

presence_columns = ['square_id', 'count', 'timeslot']
presence_data = pd.read_csv(input_path,
    names=presence_columns,
    header=None)

wt_start = datetime.datetime.strptime('08:00:00', '%H:%M:%S').time()
wt_end = datetime.datetime.strptime('18:00:00', '%H:%M:%S').time()

presence_data['datetime'] = pd.to_datetime(presence_data['timeslot'], unit='ms')
presence_data['weekday'] = presence_data['datetime'].dt.weekday
presence_data['time'] = presence_data['datetime'].dt.time

residents_condition = (((presence_data.weekday < 5) & ~((presence_data.time >= wt_start) & (presence_data.time <= wt_end))) | (presence_data.weekday > 4))
employees_condition = ((presence_data.weekday < 5) & ((presence_data.time >= wt_start) & (presence_data.time <= wt_end)))

residents = presence_data[residents_condition]\
    .groupby(['square_id'], as_index=False)\
    .agg({'count': np.mean})
residents.columns = ['square_id', 'residents']

employees = presence_data[employees_condition]\
    .groupby(['square_id'], as_index=False)\
    .agg({'count': np.mean})\
    .rename({'count': 'employees'})
employees.columns = ['square_id', 'employees']

result = pd.merge(residents, employees, how='left', on=['square_id', 'square_id'])\
    .sort_values(by='square_id', ascending=1)

result.to_csv(
        output_path,
        index=False,
        mode='w+')